package cs555.airlineanalysis.Q7;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by iyro on 10/26/15.
 */
public class LateAircraftReducer extends Reducer<Text, Text, Text, Text> {
    private MultipleOutputs<Text, Text> mos;

    @Override
    public void setup(Context context) {
        mos = new MultipleOutputs<>(context);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
    }

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        TreeMap<Long, String> departureTimeMapping = new TreeMap<>();
        for (Text value : values) {
            String[] delays = value.toString().split(",");

            Long scheduledDepTime = Long.parseLong(delays[0]);

            departureTimeMapping.put(scheduledDepTime, value.toString());
        }

        ArrayList<Long> keys = new ArrayList<>(departureTimeMapping.keySet());
        for (int i = 0; i < keys.size(); i++) {
            String[] delays = departureTimeMapping.get(keys.get(i)).split(",");

            Long aircraftDelay = Long.parseLong(delays[1]);
            Long carrierDelay = Long.parseLong(delays[2]);
            Long weatherDelay = Long.parseLong(delays[3]);
            Long NASDelay = Long.parseLong(delays[4]);
            Long securityDelay = Long.parseLong(delays[5]);

            Integer chainCount=0;

            if (aircraftDelay == 0) {
                for (int j = i + 1; j < keys.size(); j++) {
                    String[] innerDelays = departureTimeMapping.get(keys.get(j)).split(",");

                    Long inneraircraftDelay = Long.parseLong(innerDelays[1]);

                    if (inneraircraftDelay == 0) {
                        break;
                    } else {
                        chainCount++;
                        i++;
                    }
                }

                if (chainCount > 0) {
                    if (carrierDelay > 0) {
                        context.write(new Text("carrier"), new Text(chainCount.toString()));
                    }
                    if (weatherDelay > 0) {
                        context.write(new Text("weather"), new Text(chainCount.toString()));
                    }
                    if (NASDelay > 0) {
                        context.write(new Text("NAS"), new Text(chainCount.toString()));
                    }
                    if (securityDelay > 0) {
                        context.write(new Text("security"), new Text(chainCount.toString()));
                    }
                    if (aircraftDelay > 0) {
                        String[] day = key.toString().split(",");
                        String tailNum = day[0];
                        Integer year = Integer.parseInt(day[1]);
                        Integer month = Integer.parseInt(day[2]);
                        Integer date = Integer.parseInt(day[3]);
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.YEAR, year);
                        cal.set(Calendar.MONTH, month);
                        cal.set(Calendar.DATE, date);
                        cal.add(Calendar.DAY_OF_YEAR,-1);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy,M,dd");
                        String newKey = tailNum + "," + sdf.format(cal.getTime());
                        context.write(new Text(newKey), new Text(chainCount.toString()));
                    }
                }
            }
        }
    }
}
