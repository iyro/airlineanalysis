package cs555.airlineanalysis.Q7;

import cs555.airlineanalysis.utilities.CSVParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;

/**
 * Created by iyro on 10/26/15.
 */
public class LateAircraftMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        CSVParser csvParser = new CSVParser(value);
        String tailNum = csvParser.getField(10);
        String year = csvParser.getField(0);
        String month = csvParser.getField(1);
        String date = csvParser.getField(2);

        Long scheduledDepTime;
        Long aircraftDelay;
        Long carrierDelay;
        Long weatherDelay;
        Long NASDelay;
        Long securityDelay;

        try {
            scheduledDepTime = Long.parseLong(csvParser.getField(5));
            aircraftDelay = Long.parseLong(csvParser.getField(28));
            carrierDelay = Long.parseLong(csvParser.getField(24));
            weatherDelay = Long.parseLong(csvParser.getField(25));
            NASDelay = Long.parseLong(csvParser.getField(26));
            securityDelay = Long.parseLong(csvParser.getField(27));

            String sb = scheduledDepTime.toString() + "," +
                    carrierDelay.toString() + "," +
                    weatherDelay.toString() + "," +
                    NASDelay.toString() + "," +
                    securityDelay.toString() + "," +
                    aircraftDelay.toString();

            if (!tailNum.isEmpty() && !tailNum.equals("0"));
                context.write(new Text(tailNum + "," + year + "," + month + "," + date), new Text(sb));

        } catch (NumberFormatException e) {

        }
    }
}