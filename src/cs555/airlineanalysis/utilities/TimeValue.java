package cs555.airlineanalysis.utilities;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by iyro on 11/5/15.
 */
public class TimeValue {
    public static String getHour(String timeString) {
        NumberFormat FORMAT = new DecimalFormat("00");
        try {
            Integer time = Integer.parseInt(timeString);

            Integer hour = time / 100;

            if (hour == 24) {
                hour = 0;
            }

            return FORMAT.format(hour) + ":00 - " + FORMAT.format(hour) + ":59";
        } catch (NumberFormatException e) {
            return "null";
        }
    }

    public static String getWeekday(String weekDay) {
        try {
            switch (Integer.parseInt(weekDay)) {
                case 1:
                    return "Monday";
                case 2:
                    return "Tuesday";
                case 3:
                    return "Wednesday";
                case 4:
                    return "Thursday";
                case 5:
                    return "Friday";
                case 6:
                    return "Saturday";
                case 7:
                    return "Sunday";
            }
        } catch (NumberFormatException e) {
            return "null";
        }
        return "null";
    }

    public static String getMonth(String month) {
        try {
            switch (Integer.parseInt(month)) {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
            }
        } catch (NumberFormatException e) {
            return "null";
        }
        return "null";
    }
}
