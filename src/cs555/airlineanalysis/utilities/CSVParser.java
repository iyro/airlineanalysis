package cs555.airlineanalysis.utilities;

import org.apache.hadoop.io.Text;

import java.nio.charset.CharacterCodingException;

/**
 * Created by iyro on 11/5/15.
 */
public class CSVParser {
    private String[] csvFields;

    public CSVParser(Text value) {
        this.csvFields = value.toString().split(",");

        for (int i = 0; i < csvFields.length; i++)
            csvFields[i] = csvFields[i].trim();
    }

    public String getField(Integer index) {
        if (index < csvFields.length)
            return csvFields[index];
        else
            return "";
    }
}
