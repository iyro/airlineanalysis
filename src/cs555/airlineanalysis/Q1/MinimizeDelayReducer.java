package cs555.airlineanalysis.Q1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class MinimizeDelayReducer extends Reducer<Text, Text, Text, Text> {
    private MultipleOutputs<Text, Text> mos;

    @Override
    public void setup(Context context) {
        mos = new MultipleOutputs<>(context);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
    }

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Integer totalCount = 0;
        Double sumDelay = 0d;

        for (Text value : values) {
            String[] countAndDelay = value.toString().split(",");

            Integer localCount;
            Double localAvg;

            try {
                localCount = Integer.parseInt(countAndDelay[0]);
                localAvg = Double.parseDouble(countAndDelay[1]);
            } catch (NumberFormatException e) {
                continue;
            }

            totalCount += localCount;
            sumDelay += (localCount * localAvg);
        }

        Double avgDelay = sumDelay / totalCount;

        if (key.toString().startsWith("*")) {
            mos.write("hourDelay", new Text(key.toString().substring(1)), new Text(avgDelay.toString()));
        } else if (key.toString().startsWith("$")) {
            mos.write("weekdayDelay", new Text(key.toString().substring(1)), new Text(avgDelay.toString()));
        } else if (key.toString().startsWith("%")) {
            mos.write("monthDelay", new Text(key.toString().substring(1)), new Text(avgDelay.toString()));
        }
    }
}
