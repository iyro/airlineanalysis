package cs555.airlineanalysis.Q1;

import cs555.airlineanalysis.utilities.CSVParser;
import cs555.airlineanalysis.utilities.TimeValue;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class MinimizeDelayMapper extends Mapper<LongWritable, Text, Text, Text> {
    private MultipleOutputs<Text, Text> mos;

    @Override
    public void setup(Context context) {
        mos = new MultipleOutputs<>(context);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
    }

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        CSVParser csvParser = new CSVParser(value);

        String hour = TimeValue.getHour(csvParser.getField(5));
        String weekDay = TimeValue.getWeekday(csvParser.getField(3));
        String month = TimeValue.getMonth(csvParser.getField(1));

        try {
            Integer arrivalDelay;

            if (!csvParser.getField(14).equals("NA")) {
                arrivalDelay = Integer.parseInt(csvParser.getField(14));

                if (!hour.equalsIgnoreCase("null")) {
                    context.write(new Text("*" + hour), new Text("1," + arrivalDelay.toString()));
                }

                if (!weekDay.equalsIgnoreCase("null")) {
                    context.write(new Text("$" + weekDay), new Text("1," + arrivalDelay.toString()));
                }

                if (!month.equalsIgnoreCase("null")) {
                    context.write(new Text("%" + month), new Text("1," + arrivalDelay.toString()));
                }
            }
        } catch (NumberFormatException e) {

        }
    }
}