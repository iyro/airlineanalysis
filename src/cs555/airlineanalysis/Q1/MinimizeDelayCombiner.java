package cs555.airlineanalysis.Q1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class MinimizeDelayCombiner extends Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Integer totalCount = 0;
        Double sumDelay = 0d;

        for (Text value : values) {
            String[] countAndDelay = value.toString().split(",");

            Integer localCount;
            Double localAvg;

            try {
                localCount = Integer.parseInt(countAndDelay[0]);
                localAvg = Double.parseDouble(countAndDelay[1]);
            } catch (NumberFormatException e) {
                continue;
            }

            totalCount += localCount;
            sumDelay += (localCount * localAvg);
        }

        Double avgDelay = sumDelay / totalCount;

        context.write(key, new Text(totalCount + "," + avgDelay));
    }
}
