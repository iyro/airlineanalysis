package cs555.airlineanalysis.Q6;

import cs555.airlineanalysis.utilities.CSVParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;

/**
 * Created by iyro on 10/26/15.
 */
public class AircraftYearMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        CSVParser csvParser = new CSVParser(value);
        try {
            String aircraftTailNum = csvParser.getField(0).replaceAll("\"", "");
            String aircraftYear = csvParser.getField(8).replaceAll("\"", "");

            if (!aircraftYear.equals("0000") && !aircraftYear.equals("None"))
                context.write(new Text(aircraftTailNum), new Text("|" + aircraftYear));

        } catch (ArrayIndexOutOfBoundsException e) {

        }
    }
}
