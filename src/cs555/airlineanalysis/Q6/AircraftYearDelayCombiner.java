package cs555.airlineanalysis.Q6;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by iyro on 10/26/15.
 */
public class AircraftYearDelayCombiner extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        String aircraftTailNum = key.toString();
        HashMap<String, Long> yearWiseCount = new HashMap<>();
        HashMap<String, Long> yearWiseDelay = new HashMap<>();

        for (Text value : values) {
            if (value.toString().startsWith("|")) {
                context.write(key, value);
                continue;
            }

            String[] countAndDelay = value.toString().split(",");
            String year = countAndDelay[0];

            if (!yearWiseCount.containsKey(year)) {
                yearWiseCount.put(year, 0L);
                yearWiseDelay.put(year, 0L);
            }

            Long delay = Long.parseLong(countAndDelay[2]);
            yearWiseDelay.put(year, yearWiseDelay.get(year) + delay);

            Long count = Long.parseLong(countAndDelay[1]);
            yearWiseCount.put(year, yearWiseCount.get(year) + count);
        }

        for (String year : yearWiseCount.keySet()) {
            context.write(new Text(aircraftTailNum), new Text(year + "," + yearWiseCount.get(year) + "," + yearWiseDelay.get(year)));
        }
    }
}
