package cs555.airlineanalysis.Q6;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class AircraftYearAggregateReducer extends Reducer<IntWritable, Text, IntWritable, Text> {
    @Override
    public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Double totalDelay = 0D;
        Long totalCount = 0L;

        for (Text value : values) {
            String[] countAndDelay = value.toString().split(",");

            Long delay = Long.parseLong(countAndDelay[1]);
            totalDelay += delay;

            Long count = Long.parseLong(countAndDelay[0]);
            totalCount += count;
        }

        Double average = totalDelay/totalCount;
        context.write(key, new Text(average.toString()));
    }
}
