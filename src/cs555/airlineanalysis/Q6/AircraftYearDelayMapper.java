package cs555.airlineanalysis.Q6;

import cs555.airlineanalysis.utilities.CSVParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;

/**
 * Created by iyro on 10/26/15.
 */
public class AircraftYearDelayMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        CSVParser csvParser = new CSVParser(value);
        String tailNum = csvParser.getField(10);
        String year = csvParser.getField(0);
        Long aircraftDelay;
        try {
            aircraftDelay = Long.parseLong(csvParser.getField(14));
            if (aircraftDelay > 0)
                context.write(new Text(tailNum), new Text(year + "," + "1," + "1"));
            else
                context.write(new Text(tailNum), new Text(year + "," + "1," + "0"));
        } catch (NumberFormatException e) {

        }
    }
}