package cs555.airlineanalysis.Q5;

import cs555.airlineanalysis.utilities.CSVParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by iyro on 10/26/15.
 */
public class CarrierDelayMapper extends Mapper<LongWritable, Text, Text, Text> {
    private static final NumberFormat FORMAT = new DecimalFormat("00");

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        CSVParser csvParser = new CSVParser(value);
        context.write(new Text(csvParser.getField(8)), new Text("1," + csvParser.getField(24)));
    }
}