package cs555.airlineanalysis.Q5;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class CarrierDelayReducer extends Reducer<Text, Text, Text, Text> {
    private MultipleOutputs<Text, Text> mos;

    @Override
    public void setup(Context context) {
        mos = new MultipleOutputs<>(context);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
    }

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Integer totalCount = 0;
        Double sumDelay = 0d;
        String carrierName = key.toString();

        for (Text value : values) {
            if (value.toString().startsWith("|")) {
                carrierName = value.toString().substring(1);
                continue;
            }

            String[] countAndDelay = value.toString().split(",");

            Integer localCount;
            Long minutesLost;

            try {
                localCount = Integer.parseInt(countAndDelay[0]);
                minutesLost = Long.parseLong(countAndDelay[1]);
            } catch (NumberFormatException e) {
                continue;
            }

            totalCount += localCount;
            sumDelay += minutesLost;
        }

        if (totalCount != 0) {
            Double avgDelay = sumDelay / totalCount;
            mos.write("flightsDelayed", new Text(carrierName), new Text(totalCount.toString()));
            mos.write("minutesLost", new Text(carrierName), new Text(Integer.valueOf(sumDelay.intValue()).toString()));
            mos.write("averageDelay", new Text(carrierName), new Text(avgDelay.toString()));
        }
    }
}
