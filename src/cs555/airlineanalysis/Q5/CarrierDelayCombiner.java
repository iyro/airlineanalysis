package cs555.airlineanalysis.Q5;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class CarrierDelayCombiner extends Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Integer totalCount = 0;
        Long sumDelay = 0l;

        for (Text value : values) {
            if (value.toString().startsWith("|")) {
                context.write(key, value);
                continue;
            }

            String[] countAndDelay = value.toString().split(",");

            Integer localCount;
            Long minsLost;

            try {
                localCount = Integer.parseInt(countAndDelay[0]);
                minsLost = Long.parseLong(countAndDelay[1]);
            } catch (NumberFormatException e) {
                continue;
            }

            totalCount += localCount;
            sumDelay += minsLost;
        }

        context.write(key, new Text(totalCount + "," + sumDelay));
    }
}
