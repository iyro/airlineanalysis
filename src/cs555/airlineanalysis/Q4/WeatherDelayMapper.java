package cs555.airlineanalysis.Q4;

import cs555.airlineanalysis.utilities.CSVParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;

/**
 * Created by iyro on 10/26/15.
 */
public class WeatherDelayMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        CSVParser csvParser = new CSVParser(value);
        String sourceAirport = csvParser.getField(16);
        Long weatherDelay;
        try {
            weatherDelay = Long.parseLong(csvParser.getField(25));
            if (weatherDelay > 0)
                context.write(new Text(sourceAirport), new Text("1"));
        } catch (NumberFormatException e) {

        }
    }
}