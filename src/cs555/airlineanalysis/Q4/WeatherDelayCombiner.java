package cs555.airlineanalysis.Q4;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class WeatherDelayCombiner extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Long totalDelay = 0L;
        String airportCity = key.toString();

        for (Text value : values) {
            if (value.toString().startsWith("|")) {
                context.write(key, value);
                continue;
            }

            Long delay = Long.parseLong(value.toString());
            totalDelay += delay;
        }

        if (totalDelay>0)
            context.write(new Text(airportCity), new Text(totalDelay.toString()));
    }
}
