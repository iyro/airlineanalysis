package cs555.airlineanalysis.Q4;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by iyro on 10/26/15.
 */
public class WeatherDelayReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Long totalDelay = 0L;
        String airportCity = key.toString();

        for (Text value : values) {
            if (value.toString().startsWith("|")) {
                airportCity = value.toString().substring(1);
                continue;
            }

            Long delay = Long.parseLong(value.toString());
            totalDelay += delay;
        }

        if (totalDelay>0)
            context.write(new Text(airportCity), new Text(totalDelay.toString()));
    }
}
