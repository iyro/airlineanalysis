package cs555.airlineanalysis.Q3;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class RanksSortReducer extends Reducer<IntWritable, Text, Text, Text> {
    @Override
    public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        for (Text value : values) {
            int indexOfTab = value.toString().indexOf("\t");

            String airport = value.toString().substring(0, indexOfTab);
            String counts = value.toString().substring(indexOfTab+1);
            context.write(new Text(airport), new Text(counts));
        }
    }
}
