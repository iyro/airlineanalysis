package cs555.airlineanalysis.Q3;

import cs555.airlineanalysis.utilities.CSVParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;

/**
 * Created by iyro on 10/26/15.
 */
public class AirportNameMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        CSVParser csvParser = new CSVParser(value);

        String airportCode = csvParser.getField(0).replaceAll("\"", "");
        String airportName = csvParser.getField(1).replaceAll("\"", "");

        context.write(new Text(airportCode), new Text("|" + airportName));
    }
}
