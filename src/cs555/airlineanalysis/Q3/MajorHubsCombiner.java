package cs555.airlineanalysis.Q3;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by iyro on 10/26/15.
 */
public class MajorHubsCombiner extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        HashMap<String, Integer> yearCount = new HashMap<>();

        Integer year;
        for (year = 1987; year <= 2008; year++) {
            yearCount.put(year.toString(), 0);
        }

        for (Text value : values) {
            if (value.toString().startsWith("|")) {
                context.write(key, value);
                continue;
            }

            String[] countAndYear = value.toString().split(",");
            Integer count = Integer.parseInt(countAndYear[0]);
            yearCount.put(countAndYear[1], yearCount.get(countAndYear[1]) + count);
        }

        for (String hashKey : yearCount.keySet()) {
            if (yearCount.get(hashKey) > 0)
                context.write(key, new Text(yearCount.get(hashKey).toString() + "," + hashKey));
        }
    }
}
