package cs555.airlineanalysis.Q3;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.*;

public class MajorHubsAggregateReducer extends Reducer<IntWritable, Text, Text, Text> {
    private MultipleOutputs<Text, Text> mos;

    @Override
    public void setup(Context context) {
        mos = new MultipleOutputs<>(context);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
    }

    @Override
    public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        HashMap<String, HashMap<String, Integer>> yearWiseCounts = new HashMap<>();

        Integer year;
        for (year = 1987; year<=2008; year++) {
            yearWiseCounts.put(year.toString(), new HashMap<String, Integer>());
        }

        HashMap<String, HashMap<Integer, Integer>> allRanks = new HashMap<>();

        for (Text value : values) {
            int indexofPipe = value.toString().indexOf("|");
            String airportName = value.toString().substring(0, indexofPipe);

            String[] counts = value.toString().substring(indexofPipe+1).split(",");

            Integer index;
            for (index = 0; index<counts.length; index++) {
                year = index + 1987;
                yearWiseCounts.get(year.toString()).put(airportName, Integer.parseInt(counts[index]));
            }

            allRanks.put(airportName, new HashMap<Integer, Integer>());
        }

        for (String yearValue : yearWiseCounts.keySet()) {
            Set<Map.Entry<String, Integer>> set = yearWiseCounts.get(yearValue).entrySet();
            List<Map.Entry<String, Integer>> list = new ArrayList<>(set);
            Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
            {
                public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
                {
                    return (o2.getValue()).compareTo( o1.getValue() );
                }
            } );

            for (String airport : allRanks.keySet()) {
                for (int index = 0; index<list.size(); index++) {
                    if (list.get(index).getKey().equals(airport)) {
                        allRanks.get(airport).put(Integer.parseInt(yearValue), index+1);
                        break;
                    }
                }
            }
        }

        for (String airport : allRanks.keySet()) {
            context.write(new Text(airport), new Text(getCSStringFromHashMap(allRanks.get(airport))));
        }
    }

    private String getCSStringFromHashMap(HashMap<Integer, Integer> yearCount) {
        StringBuilder sb = new StringBuilder();

        Integer year;
        for (year = 1987; year <= 2008; year++) {
            sb.append(yearCount.get(year).toString());
            if (year != 2008)
                sb.append(",");
        }
        return sb.toString();
    }
}
