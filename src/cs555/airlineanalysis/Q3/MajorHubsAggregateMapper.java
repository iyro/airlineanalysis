package cs555.airlineanalysis.Q3;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class MajorHubsAggregateMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] keyValue = value.toString().split("\\t");

        //TODO make same key
        context.write(new IntWritable(1), new Text(keyValue[0] + "|" + keyValue[1]));
    }
}
