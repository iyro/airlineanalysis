package cs555.airlineanalysis.Q3;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class RanksSortMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] keyValue = value.toString().split("\\t");

        String[] counts = keyValue[1].split(",");
        Integer count = Integer.parseInt(counts[counts.length - 1]);

        context.write(new IntWritable(count), new Text(value));
    }
}
