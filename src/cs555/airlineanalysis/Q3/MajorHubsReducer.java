package cs555.airlineanalysis.Q3;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by iyro on 10/26/15.
 */
public class MajorHubsReducer extends Reducer<Text, Text, Text, Text> {
    //private MultipleOutputs<Text, Text> mos;

    /*@Override
    public void setup(Context context) {
        mos = new MultipleOutputs<>(context);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
    }*/

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Integer totalCount = 0;
        String airportName = key.toString();
        HashMap<String, Integer> yearCount = new HashMap<>();

        Integer year;
        for (year = 1987; year <= 2008; year++) {
            yearCount.put(year.toString(), 0);
        }

        for (Text value : values) {
            if (value.toString().startsWith("|")) {
                airportName = value.toString().substring(1);
                continue;
            }

            String[] countAndYear = value.toString().split(",");
            Integer count = Integer.parseInt(countAndYear[0]);
            totalCount += count;
            yearCount.put(countAndYear[1], yearCount.get(countAndYear[1]) + count);
        }

        if (totalCount>0)
            context.write(new Text(airportName), new Text(getCSStringFromHashMap(yearCount)));
    }

    private String getCSStringFromHashMap(HashMap<String, Integer> yearCount) {
        StringBuilder sb = new StringBuilder();

        Integer year;
        for (year = 1987; year <= 2008; year++) {
            sb.append(yearCount.get(year.toString()).toString());
            if (year != 2008)
                sb.append(",");
        }
        return sb.toString();
    }
}
