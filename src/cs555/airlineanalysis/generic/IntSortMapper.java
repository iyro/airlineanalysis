package cs555.airlineanalysis.generic;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class IntSortMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] keyValue = value.toString().split("\\t");

        Integer count = Integer.parseInt(keyValue[1]);

        context.write(new IntWritable(count), new Text(keyValue[0]));
    }
}
