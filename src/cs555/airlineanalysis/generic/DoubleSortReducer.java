package cs555.airlineanalysis.generic;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class DoubleSortReducer extends Reducer<DoubleWritable, Text, Text, Text> {
    @Override
    public void reduce(DoubleWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        for (Text value : values) {
            context.write(value, new Text(key.toString()));
        }
    }
}
