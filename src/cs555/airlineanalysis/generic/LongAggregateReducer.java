package cs555.airlineanalysis.generic;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class LongAggregateReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Long totalDelay = 0L;
        for (Text value : values) {
            Long delay = Long.parseLong(value.toString());
            totalDelay += delay;
        }
        context.write(key, new Text(totalDelay.toString()));
    }
}
