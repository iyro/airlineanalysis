package cs555.airlineanalysis.generic;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class IntDescendingSortComparator extends WritableComparator {
    protected IntDescendingSortComparator() {
        super(IntWritable.class, true);
    }

    @Override
    public int compare(WritableComparable w1, WritableComparable w2) {
        IntWritable k1 = (IntWritable) w1;
        IntWritable k2 = (IntWritable) w2;

        return k2.compareTo(k1);
    }
}
