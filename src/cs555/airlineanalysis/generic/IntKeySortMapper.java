package cs555.airlineanalysis.generic;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class IntKeySortMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] keyValue = value.toString().split("\\t");

        Integer age = Integer.parseInt(keyValue[0]);

        context.write(new IntWritable(age), new Text(keyValue[1]));
    }
}
