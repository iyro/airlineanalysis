package cs555.airlineanalysis.generic;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by iyro on 10/26/15.
 */
public class DoubleSortMapper extends Mapper<LongWritable, Text, DoubleWritable, Text> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] keyValue = value.toString().split("\\t");

        //String[] countAndDelay = keyValue[1].split(",");

        Double average = Double.parseDouble(keyValue[1]);

        context.write(new DoubleWritable(average), new Text(keyValue[0]));
    }
}
