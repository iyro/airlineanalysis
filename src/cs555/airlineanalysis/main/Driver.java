package cs555.airlineanalysis.main;

import cs555.airlineanalysis.Q1.MinimizeDelayCombiner;
import cs555.airlineanalysis.Q1.MinimizeDelayMapper;
import cs555.airlineanalysis.Q1.MinimizeDelayReducer;
import cs555.airlineanalysis.Q3.*;
import cs555.airlineanalysis.Q4.AirportCityMapper;
import cs555.airlineanalysis.Q4.WeatherDelayCombiner;
import cs555.airlineanalysis.Q4.WeatherDelayMapper;
import cs555.airlineanalysis.Q4.WeatherDelayReducer;
import cs555.airlineanalysis.Q5.CarrierDelayCombiner;
import cs555.airlineanalysis.Q5.CarrierDelayMapper;
import cs555.airlineanalysis.Q5.CarrierDelayReducer;
import cs555.airlineanalysis.Q5.CarrierNameMapper;
import cs555.airlineanalysis.Q6.*;
import cs555.airlineanalysis.Q7.LateAircraftMapper;
import cs555.airlineanalysis.Q7.LateAircraftReducer;
import cs555.airlineanalysis.generic.DoubleDescendingSortComparator;
import cs555.airlineanalysis.generic.IntDescendingSortComparator;
import cs555.airlineanalysis.generic.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

public class Driver extends Configured implements Tool {
    private static String[] MAIN_DATASET = new String[22];
    private static String AIRPORTS_DATASET = "/data/supplementary/airports.csv";
    private static String CARRIERS_DATASET = "/data/supplementary/carriers.csv";
    private static String PLANE_DATA = "/data/supplementary/plane-data.csv";
    private static String OUTPUT_FILE = "/home/AirlineAnalysis";

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new Driver(), args));
    }

    @Override
    public int run(String[] args) throws Exception {
        Integer year;
        for (year = 1987; year <= 2008; year++) {
            MAIN_DATASET[year-1987] = "/data/main/" + year + ".csv";
        }

        boolean isDone;

        /*//Q1 Q2 Begin
        isDone = minimizeDelays();
        if (!isDone) return 1;

        isDone = minimizeDelaySort("hourDelay");
        if (!isDone) return 1;

        isDone = minimizeDelaySort("weekdayDelay");
        if (!isDone) return 1;

        isDone = minimizeDelaySort("monthDelay");
        if (!isDone) return 1;

        deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q1_I");
        //Q1 Q2 End

        //Q3 Begins
        isDone = majorHubs();
        if (!isDone) return 1;

        isDone = majorHubsAggregate();
        if (!isDone) return 1;

        deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q3_I");
        isDone = majorHubsSort();
        if (!isDone) return 1;

        deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q3_IC");
        //Q3 Ends*/

        /*//Q4 Begins
        isDone = weatherDelays();
        if (!isDone) return 1;

        isDone = weatherDelayAggregate();
        if (!isDone) return 1;

        deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q4_I");

        isDone = weatherDelaySort();
        if (!isDone) return 1;

        deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q4_IC");
        //Q4 Ends

        //Q5 Begins
        isDone = carrierDelays();
        if (!isDone) return 1;

        isDone = carrierDelaySort("flightsDelayed");
        if (!isDone) return 1;

        isDone = carrierDelaySort("minutesLost");
        if (!isDone) return 1;

        isDone = carrierDelaySort("averageDelay");
        if (!isDone) return 1;

        deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q5_I");
        //Q5 Ends*/

        /*//Q6 Begins
        isDone = aircraftYearDelays();
        if (!isDone) return 1;

        isDone = aircraftYearAggregate();
        if (!isDone) return 1;

        //deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q6_I");
        //Q6 Ends*/

        //Q7 Begins
        isDone = lateAircraftCauses();
        if (!isDone) return 1;

        isDone = lateAircraftCausesAggregate();
        if (!isDone) return 1;

        isDone = lateAircraftCausesSort();
        if (!isDone) return 1;

        //deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q6_I");
        //Q7 Ends
        return 0;
    }

    private boolean minimizeDelays() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job minimizeDelayJob = Job.getInstance(conf, "minimizeDelay");
        minimizeDelayJob.setJarByClass(Driver.class);

        for (String inputFile : MAIN_DATASET) {
            MultipleInputs.addInputPath(minimizeDelayJob, new Path(inputFile), TextInputFormat.class, MinimizeDelayMapper.class);
        }

        FileOutputFormat.setOutputPath(minimizeDelayJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q1_I"));

        MultipleOutputs.addNamedOutput(minimizeDelayJob, "hourDelay", TextOutputFormat.class, Text.class, Text.class);
        MultipleOutputs.addNamedOutput(minimizeDelayJob, "weekdayDelay", TextOutputFormat.class, Text.class, Text.class);
        MultipleOutputs.addNamedOutput(minimizeDelayJob, "monthDelay", TextOutputFormat.class, Text.class, Text.class);

        LazyOutputFormat.setOutputFormatClass(minimizeDelayJob, TextOutputFormat.class);

        minimizeDelayJob.setMapOutputKeyClass(Text.class);

        minimizeDelayJob.setReducerClass(MinimizeDelayReducer.class);

        minimizeDelayJob.setCombinerClass(MinimizeDelayCombiner.class);

        minimizeDelayJob.setOutputKeyClass(Text.class);
        minimizeDelayJob.setOutputValueClass(Text.class);

        return minimizeDelayJob.waitForCompletion(true);
    }

    private boolean minimizeDelaySort(String type) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job minimizeDelaySortJob = Job.getInstance(conf, "minimizeDelaySort");
        minimizeDelaySortJob.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(minimizeDelaySortJob, new Path(OUTPUT_FILE + "_Q1_I/" + type + "-r-00000"));

        FileOutputFormat.setOutputPath(minimizeDelaySortJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q1_" + type));

        minimizeDelaySortJob.setOutputFormatClass(TextOutputFormat.class);

        minimizeDelaySortJob.setMapperClass(DoubleSortMapper.class);
        minimizeDelaySortJob.setMapOutputKeyClass(DoubleWritable.class);

        minimizeDelaySortJob.setReducerClass(DoubleSortReducer.class);

        minimizeDelaySortJob.setOutputKeyClass(Text.class);
        minimizeDelaySortJob.setOutputValueClass(Text.class);

        return minimizeDelaySortJob.waitForCompletion(true);
    }

    private boolean majorHubs() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job majorHubsJob = Job.getInstance(conf, "majorHubs");
        majorHubsJob.setJarByClass(Driver.class);

        for (String inputFile : MAIN_DATASET) {
            MultipleInputs.addInputPath(majorHubsJob, new Path(inputFile), TextInputFormat.class, MajorHubsMapper.class);
        }

        MultipleInputs.addInputPath(majorHubsJob, new Path(AIRPORTS_DATASET), TextInputFormat.class, AirportNameMapper.class);

        FileOutputFormat.setOutputPath(majorHubsJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q3_I"));

        LazyOutputFormat.setOutputFormatClass(majorHubsJob, TextOutputFormat.class);

        majorHubsJob.setMapOutputKeyClass(Text.class);

        majorHubsJob.setCombinerClass(MajorHubsCombiner.class);

        majorHubsJob.setReducerClass(MajorHubsReducer.class);

        majorHubsJob.setOutputKeyClass(Text.class);
        majorHubsJob.setOutputValueClass(Text.class);

        return majorHubsJob.waitForCompletion(true);
    }

    private boolean majorHubsAggregate() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job majorHubsAggregate = Job.getInstance(conf, "majorHubsAggregate");
        majorHubsAggregate.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(majorHubsAggregate, new Path(OUTPUT_FILE + "_Q3_I"));

        FileOutputFormat.setOutputPath(majorHubsAggregate, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q3_IC"));

        MultipleOutputs.addNamedOutput(majorHubsAggregate, "error", TextOutputFormat.class, Text.class, Text.class);
        MultipleOutputs.addNamedOutput(majorHubsAggregate, "records", TextOutputFormat.class, Text.class, Text.class);

        majorHubsAggregate.setOutputFormatClass(TextOutputFormat.class);

        majorHubsAggregate.setMapperClass(MajorHubsAggregateMapper.class);
        majorHubsAggregate.setMapOutputKeyClass(IntWritable.class);

        majorHubsAggregate.setReducerClass(MajorHubsAggregateReducer.class);

        majorHubsAggregate.setOutputKeyClass(Text.class);
        majorHubsAggregate.setOutputValueClass(Text.class);

        return majorHubsAggregate.waitForCompletion(true);
    }

    private boolean majorHubsSort() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job majorHubsSortJob = Job.getInstance(conf, "majorHubsSort");
        majorHubsSortJob.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(majorHubsSortJob, new Path(OUTPUT_FILE + "_Q3_IC"));
        FileOutputFormat.setOutputPath(majorHubsSortJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q3"));

        majorHubsSortJob.setOutputFormatClass(TextOutputFormat.class);

        majorHubsSortJob.setMapperClass(RanksSortMapper.class);
        majorHubsSortJob.setMapOutputKeyClass(IntWritable.class);

        majorHubsSortJob.setReducerClass(RanksSortReducer.class);

        majorHubsSortJob.setOutputKeyClass(Text.class);
        majorHubsSortJob.setOutputValueClass(Text.class);

        return majorHubsSortJob.waitForCompletion(true);
    }

    private boolean weatherDelays() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job weatherDelayJob = Job.getInstance(conf, "weatherDelay");
        weatherDelayJob.setJarByClass(Driver.class);

        for (String inputFile : MAIN_DATASET) {
            MultipleInputs.addInputPath(weatherDelayJob, new Path(inputFile), TextInputFormat.class, WeatherDelayMapper.class);
        }

        MultipleInputs.addInputPath(weatherDelayJob, new Path(AIRPORTS_DATASET), TextInputFormat.class, AirportCityMapper.class);

        FileOutputFormat.setOutputPath(weatherDelayJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q4_I"));

        weatherDelayJob.setOutputFormatClass(TextOutputFormat.class);

        weatherDelayJob.setMapOutputKeyClass(Text.class);

        weatherDelayJob.setCombinerClass(WeatherDelayCombiner.class);
        weatherDelayJob.setReducerClass(WeatherDelayReducer.class);

        weatherDelayJob.setOutputKeyClass(Text.class);
        weatherDelayJob.setOutputValueClass(Text.class);

        return weatherDelayJob.waitForCompletion(true);
    }

    private boolean weatherDelayAggregate() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job weatherDelayAggregateJob = Job.getInstance(conf, "weatherDelayAggregate");
        weatherDelayAggregateJob.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(weatherDelayAggregateJob, new Path(OUTPUT_FILE + "_Q4_I"));

        FileOutputFormat.setOutputPath(weatherDelayAggregateJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q4_IC"));

        weatherDelayAggregateJob.setOutputFormatClass(TextOutputFormat.class);

        weatherDelayAggregateJob.setMapperClass(TabSeparatedMapper.class);
        weatherDelayAggregateJob.setMapOutputKeyClass(Text.class);

        weatherDelayAggregateJob.setReducerClass(LongAggregateReducer.class);

        weatherDelayAggregateJob.setOutputKeyClass(Text.class);
        weatherDelayAggregateJob.setOutputValueClass(Text.class);

        return weatherDelayAggregateJob.waitForCompletion(true);
    }

    private boolean weatherDelaySort() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job weatherDelaySortJob = Job.getInstance(conf, "weatherDelaySort");
        weatherDelaySortJob.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(weatherDelaySortJob, new Path(OUTPUT_FILE + "_Q4_IC"));
        FileOutputFormat.setOutputPath(weatherDelaySortJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q4"));

        weatherDelaySortJob.setOutputFormatClass(TextOutputFormat.class);

        weatherDelaySortJob.setMapperClass(IntSortMapper.class);
        weatherDelaySortJob.setMapOutputKeyClass(IntWritable.class);

        weatherDelaySortJob.setSortComparatorClass(IntDescendingSortComparator.class);

        weatherDelaySortJob.setReducerClass(IntSortReducer.class);

        weatherDelaySortJob.setOutputKeyClass(Text.class);
        weatherDelaySortJob.setOutputValueClass(Text.class);

        return weatherDelaySortJob.waitForCompletion(true);
    }

    private boolean carrierDelays() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job carrierDelayJob = Job.getInstance(conf, "carrierDelay");
        carrierDelayJob.setJarByClass(Driver.class);

        for (String inputFile : MAIN_DATASET) {
            MultipleInputs.addInputPath(carrierDelayJob, new Path(inputFile), TextInputFormat.class, CarrierDelayMapper.class);
        }

        MultipleInputs.addInputPath(carrierDelayJob, new Path(CARRIERS_DATASET), TextInputFormat.class, CarrierNameMapper.class);

        FileOutputFormat.setOutputPath(carrierDelayJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q5_I"));

        MultipleOutputs.addNamedOutput(carrierDelayJob, "flightsDelayed", TextOutputFormat.class, Text.class, Text.class);
        MultipleOutputs.addNamedOutput(carrierDelayJob, "minutesLost", TextOutputFormat.class, Text.class, Text.class);
        MultipleOutputs.addNamedOutput(carrierDelayJob, "averageDelay", TextOutputFormat.class, Text.class, Text.class);

        LazyOutputFormat.setOutputFormatClass(carrierDelayJob, TextOutputFormat.class);

        carrierDelayJob.setMapOutputKeyClass(Text.class);

        carrierDelayJob.setReducerClass(CarrierDelayReducer.class);

        carrierDelayJob.setCombinerClass(CarrierDelayCombiner.class);

        carrierDelayJob.setOutputKeyClass(Text.class);
        carrierDelayJob.setOutputValueClass(Text.class);

        return carrierDelayJob.waitForCompletion(true);
    }

    private boolean carrierDelaySort(String type) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job carrierDelaySort = Job.getInstance(conf, "carrierDelaySort");
        carrierDelaySort.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(carrierDelaySort, new Path(OUTPUT_FILE + "_Q5_I/" + type + "-r-00000"));

        FileOutputFormat.setOutputPath(carrierDelaySort, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q5_" + type));

        carrierDelaySort.setOutputFormatClass(TextOutputFormat.class);

        if (!type.equals("averageDelay")) {
            carrierDelaySort.setMapperClass(IntSortMapper.class);
            carrierDelaySort.setMapOutputKeyClass(IntWritable.class);
            carrierDelaySort.setReducerClass(IntSortReducer.class);
            carrierDelaySort.setSortComparatorClass(IntDescendingSortComparator.class);
        } else {
            carrierDelaySort.setMapperClass(DoubleSortMapper.class);
            carrierDelaySort.setMapOutputKeyClass(DoubleWritable.class);
            carrierDelaySort.setReducerClass(DoubleSortReducer.class);
            carrierDelaySort.setSortComparatorClass(DoubleDescendingSortComparator.class);
        }

        carrierDelaySort.setOutputKeyClass(Text.class);
        carrierDelaySort.setOutputValueClass(Text.class);

        return carrierDelaySort.waitForCompletion(true);
    }

    private boolean aircraftYearDelays() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job aircraftYearDelayJob = Job.getInstance(conf, "aircraftYearDelay");
        aircraftYearDelayJob.setJarByClass(Driver.class);

        for (String inputFile : MAIN_DATASET) {
            MultipleInputs.addInputPath(aircraftYearDelayJob, new Path(inputFile), TextInputFormat.class, AircraftYearDelayMapper.class);
        }

        MultipleInputs.addInputPath(aircraftYearDelayJob, new Path(PLANE_DATA), TextInputFormat.class, AircraftYearMapper.class);

        FileOutputFormat.setOutputPath(aircraftYearDelayJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q6_I"));

        aircraftYearDelayJob.setOutputFormatClass(TextOutputFormat.class);

        aircraftYearDelayJob.setMapOutputKeyClass(Text.class);

        aircraftYearDelayJob.setCombinerClass(AircraftYearDelayCombiner.class);
        aircraftYearDelayJob.setReducerClass(AircraftYearDelayReducer.class);

        aircraftYearDelayJob.setOutputKeyClass(Text.class);
        aircraftYearDelayJob.setOutputValueClass(Text.class);

        return aircraftYearDelayJob.waitForCompletion(true);
    }

    private boolean aircraftYearAggregate() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job aircraftYearAggregate = Job.getInstance(conf, "aircraftYearAggregate");
        aircraftYearAggregate.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(aircraftYearAggregate, new Path(OUTPUT_FILE + "_Q6_I"));

        FileOutputFormat.setOutputPath(aircraftYearAggregate, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q6"));

        aircraftYearAggregate.setOutputFormatClass(TextOutputFormat.class);

        aircraftYearAggregate.setMapperClass(IntKeySortMapper.class);
        aircraftYearAggregate.setMapOutputKeyClass(IntWritable.class);

        aircraftYearAggregate.setReducerClass(AircraftYearAggregateReducer.class);

        aircraftYearAggregate.setOutputKeyClass(IntWritable.class);
        aircraftYearAggregate.setOutputValueClass(Text.class);

        return aircraftYearAggregate.waitForCompletion(true);
    }

    private boolean lateAircraftCauses() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job lateAircraftCausesJob = Job.getInstance(conf, "lateAircraftCausesJob");
        lateAircraftCausesJob.setJarByClass(Driver.class);

        for (String inputFile : MAIN_DATASET) {
            MultipleInputs.addInputPath(lateAircraftCausesJob, new Path(inputFile), TextInputFormat.class, LateAircraftMapper.class);
        }

        FileOutputFormat.setOutputPath(lateAircraftCausesJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q7_IC"));

        lateAircraftCausesJob.setOutputFormatClass(TextOutputFormat.class);

        lateAircraftCausesJob.setMapOutputKeyClass(Text.class);

        lateAircraftCausesJob.setReducerClass(LateAircraftReducer.class);

        lateAircraftCausesJob.setOutputKeyClass(Text.class);
        lateAircraftCausesJob.setOutputValueClass(Text.class);

        return lateAircraftCausesJob.waitForCompletion(true);
    }

    private boolean lateAircraftCausesSort() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job lateAircraftCausesSortJob = Job.getInstance(conf, "lateAircraftCausesSort");
        lateAircraftCausesSortJob.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(lateAircraftCausesSortJob, new Path(OUTPUT_FILE + "_Q7_I"));
        FileOutputFormat.setOutputPath(lateAircraftCausesSortJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q7"));

        lateAircraftCausesSortJob.setOutputFormatClass(TextOutputFormat.class);

        lateAircraftCausesSortJob.setMapperClass(IntSortMapper.class);
        lateAircraftCausesSortJob.setMapOutputKeyClass(IntWritable.class);

        lateAircraftCausesSortJob.setSortComparatorClass(IntDescendingSortComparator.class);

        lateAircraftCausesSortJob.setReducerClass(IntSortReducer.class);

        lateAircraftCausesSortJob.setOutputKeyClass(Text.class);
        lateAircraftCausesSortJob.setOutputValueClass(Text.class);

        return lateAircraftCausesSortJob.waitForCompletion(true);
    }

    private boolean lateAircraftCausesAggregate() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job lateAircraftCausesAggregateJob = Job.getInstance(conf, "lateAircraftCausesAggregate");
        lateAircraftCausesAggregateJob.setJarByClass(Driver.class);

        FileInputFormat.addInputPath(lateAircraftCausesAggregateJob, new Path(OUTPUT_FILE + "_Q7_IC"));

        FileOutputFormat.setOutputPath(lateAircraftCausesAggregateJob, deleteIfExistsAndGetPath(OUTPUT_FILE + "_Q7_I"));

        lateAircraftCausesAggregateJob.setOutputFormatClass(TextOutputFormat.class);

        lateAircraftCausesAggregateJob.setMapperClass(TabSeparatedMapper.class);
        lateAircraftCausesAggregateJob.setMapOutputKeyClass(Text.class);

        lateAircraftCausesAggregateJob.setReducerClass(LongAggregateReducer.class);

        lateAircraftCausesAggregateJob.setOutputKeyClass(Text.class);
        lateAircraftCausesAggregateJob.setOutputValueClass(Text.class);

        return lateAircraftCausesAggregateJob.waitForCompletion(true);
    }

    private Path deleteIfExistsAndGetPath(String file) throws IOException {
        FileSystem fs = FileSystem.get(getConf());
        Path p = new Path(file);
        if (fs.exists(p)) fs.delete(p, true);
        return p;
    }
}
